# How to contribute?

1. Report bugs: in the issue tracker, using relevant templates in `.gitlab` folder
2. Request features: in the issue tracker, using relevant templates in `.gitlab` folder
3. Get coding: create an issue first, then create a branch and merge request for your feature before getting started

# Your first code contribution

This code base is young. We need everyone on board and all the asssitance we can get! There are simple issues for beginners, and more complex issues that might require collaboration, we've listed a few ideas to get started:

1. Fix the graph tooltips - due to the data being decimated, there's an issue with the x-axis label (see the relevant issue and branch)
2. Processing all of this data is intensive, especially for the plotting. To fix this we've used a lot of mathy solutions (decimation, downsampling, interpolation, and memoization). We could do with some more hands on deck to make the UX frictionless.
3. Know any good UI designers? We can make this app more pretty, but we need your help!
4. Curve parameter controls, sliders, and live updates
5. This is a blockchain project, maybe we can put it on IPFS? It's all client side code.